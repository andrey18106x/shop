# GFL Graduate Work

Andrey's Borysenko graduate work.

## Tech stack

### Backend technologies

| Name        | Version      |
| :---------- | :----------- |
| Node.js     | v14.16.0     |
| Express.js  | v4.17.1      |
| MySQL       | MariaDB-10.4 |



### Frontend technologies

| Name        | Version     |
| :---------- | :---------- |
| Node.js     | v14.16.0    |
| Angular     | v11.0.3     |


## How to start

Instruction for launching the application are located separately on their pages ([backend](/backend) & [frontend](/frontend)). The backend application needs to be configured first.