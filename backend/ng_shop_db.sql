SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `ng_shop_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ng_shop_db`;

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `account_id` int(11) NOT NULL,
  `account_type` int(11) DEFAULT NULL,
  `account_date_added` datetime DEFAULT NULL,
  `account_owner` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `accounts`;
INSERT INTO `accounts` (`account_id`, `account_type`, `account_date_added`, `account_owner`, `email`, `password`, `token`) VALUES
(1, 3, '2021-04-29 00:00:00', 1, 'andrey18106x@gmail.com', '9a7bf876978ef8a0b2061eb805877895', 'c52c524dfcda145f6769222c7f598d06');

DROP TABLE IF EXISTS `account_types`;
CREATE TABLE `account_types` (
  `account_type_id` int(11) NOT NULL,
  `account_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `account_types`;
INSERT INTO `account_types` (`account_type_id`, `account_type_name`) VALUES
(1, 'Покупатель'),
(2, 'Продавец'),
(3, 'Администратор'),
(4, 'Модератор'),
(5, 'Менеджер службы доставки');

DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `options` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `cart`;
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `categories`;
INSERT INTO `categories` (`category_id`, `category_name`, `category_description`) VALUES
(1, 'Авто, мото', 'Для мотоциклов'),
(2, 'Айтишникам', 'Для заядлых гиков своего дела'),
(3, 'Интернет приколы', 'Мемы из интернета'),
(4, 'Музыка', 'Принты с музыкальными группами'),
(5, 'Праздники', 'Праздничные товары'),
(6, 'Юмор', 'Юмористические товары');

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `currency_id` int(11) NOT NULL,
  `currency_name` varchar(255) DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `currencies`;
INSERT INTO `currencies` (`currency_id`, `currency_name`, `currency_code`) VALUES
(1, 'Гривня', 'UAN'),
(2, 'Доллар', 'USD'),
(3, 'Рубль', 'RUB');

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `medium_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `address` mediumtext DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `customers`;
INSERT INTO `customers` (`customer_id`, `first_name`, `last_name`, `medium_name`, `gender`, `age`, `birthday`, `date_added`, `country`, `address`, `zip_code`, `phone`) VALUES
(1, 'Андрей', 'Борисенко', 'Александрович', 'male', 20, '2000-11-21', '2021-04-29 00:00:00', 'Украина', 'Новая Каховка, Историческая', '74900', '+380990969438');

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `customer` int(11) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `payment` int(11) DEFAULT NULL,
  `shipping` int(11) DEFAULT NULL,
  `order_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `orders`;
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details` (
  `order_detail_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `options` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `order_details`;
DROP TABLE IF EXISTS `order_statuses`;
CREATE TABLE `order_statuses` (
  `order_status_id` int(11) NOT NULL,
  `order_status_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `order_statuses`;
INSERT INTO `order_statuses` (`order_status_id`, `order_status_name`) VALUES
(1, 'Ожидает подтверждения'),
(2, 'Подтверждён'),
(3, 'Оплачен'),
(4, 'Доставлен'),
(5, 'Получен'),
(6, 'Отменён');

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `payment_name` varchar(255) DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `payment_currency` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `payment`;
INSERT INTO `payment` (`payment_id`, `payment_name`, `payment_type`, `payment_currency`) VALUES
(1, 'Наложенный платёж', 1, 1),
(2, 'Наложенный платёж', 2, 1),
(3, 'Оплата на месте', 3, 1);

DROP TABLE IF EXISTS `payment_types`;
CREATE TABLE `payment_types` (
  `payment_type_id` int(11) NOT NULL,
  `payment_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `payment_types`;
INSERT INTO `payment_types` (`payment_type_id`, `payment_type_name`) VALUES
(1, 'Наличные'),
(2, 'Безналичные'),
(3, 'Банковский перевод');

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_serial_code` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_provider` int(11) DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `product_description` varchar(255) DEFAULT NULL,
  `product_material` int(11) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `products`;
INSERT INTO `products` (`product_id`, `product_serial_code`, `product_name`, `product_provider`, `product_price`, `product_description`, `product_material`, `product_image`) VALUES
(1, '001', 'Футболка 1', 1, 400, 'Обычная футболка без рисунков', 1, 'img/products/tshirt-image.jpg'),
(2, '002', 'Футболка 2', 2, 450, 'Обычная футболка без рисунков', 2, NULL);

DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE `product_categories` (
  `product_category_id` int(11) NOT NULL,
  `product` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `product_categories`;
INSERT INTO `product_categories` (`product_category_id`, `product`, `category`) VALUES
(1, 1, 2),
(2, 2, 2);

DROP TABLE IF EXISTS `product_colors`;
CREATE TABLE `product_colors` (
  `product_color_id` int(11) NOT NULL,
  `product_color_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `product_colors`;
INSERT INTO `product_colors` (`product_color_id`, `product_color_name`) VALUES
(1, 'Красный'),
(2, 'Черный'),
(3, 'Белый'),
(4, 'Оранжевый'),
(5, 'Желтый'),
(6, 'Серый'),
(7, 'Фиолетовый');

DROP TABLE IF EXISTS `product_materials`;
CREATE TABLE `product_materials` (
  `material_id` int(11) NOT NULL,
  `material_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `product_materials`;
INSERT INTO `product_materials` (`material_id`, `material_name`) VALUES
(1, 'Хлопок 100%'),
(2, 'Хлопок 90%, полиэстер 10%'),
(3, 'Хлопок 95%, вискоза 5%');

DROP TABLE IF EXISTS `product_options`;
CREATE TABLE `product_options` (
  `option_id` int(11) NOT NULL,
  `product` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `available` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `product_options`;
INSERT INTO `product_options` (`option_id`, `product`, `type`, `color`, `size`, `available`) VALUES
(1, 1, 1, 2, 2, 5),
(2, 1, 1, 2, 3, 10),
(3, 1, 1, 6, 2, 7),
(4, 1, 1, 6, 3, 15),
(5, 1, 1, 3, 4, 20),
(6, 2, 1, 1, 2, 10);

DROP TABLE IF EXISTS `product_providers`;
CREATE TABLE `product_providers` (
  `product_provider_id` int(11) NOT NULL,
  `product_provider_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `product_providers`;
INSERT INTO `product_providers` (`product_provider_id`, `product_provider_name`) VALUES
(1, 'Поставщик 1'),
(2, 'Поставщик 2');

DROP TABLE IF EXISTS `product_sizes`;
CREATE TABLE `product_sizes` (
  `product_size_id` int(11) NOT NULL,
  `product_size_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `product_sizes`;
INSERT INTO `product_sizes` (`product_size_id`, `product_size_name`) VALUES
(1, 'S'),
(2, 'M'),
(3, 'L'),
(4, 'XL');

DROP TABLE IF EXISTS `product_types`;
CREATE TABLE `product_types` (
  `product_type_id` int(11) NOT NULL,
  `product_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `product_types`;
INSERT INTO `product_types` (`product_type_id`, `product_type_name`) VALUES
(1, 'Мужская'),
(2, 'Унисекс'),
(3, 'Женская'),
(4, 'Детская'),
(5, 'Длинный рукав'),
(6, 'Ringer');

DROP TABLE IF EXISTS `shippers`;
CREATE TABLE `shippers` (
  `shipper_id` int(11) NOT NULL,
  `shipper_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `shippers`;
INSERT INTO `shippers` (`shipper_id`, `shipper_name`) VALUES
(1, 'Новая Почта'),
(2, 'Укрпочта'),
(3, 'ДоставкаМагазина'),
(4, 'ПриватПочтомат');

DROP TABLE IF EXISTS `shipping`;
CREATE TABLE `shipping` (
  `shipping_id` int(11) NOT NULL,
  `shipper` int(11) DEFAULT NULL,
  `shipping_type` int(11) DEFAULT NULL,
  `shipping_price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `shipping`;
INSERT INTO `shipping` (`shipping_id`, `shipper`, `shipping_type`, `shipping_price`) VALUES
(1, 1, 1, 50),
(2, 1, 2, 20),
(3, 1, 3, 35),
(4, 2, 1, 40),
(5, 2, 2, 10),
(6, 2, 3, 25),
(7, 4, 2, 15);

DROP TABLE IF EXISTS `shipping_types`;
CREATE TABLE `shipping_types` (
  `shipping_type_id` int(11) NOT NULL,
  `shipping_type_name` varchar(255) DEFAULT NULL,
  `shipping_type_description` varchar(255) DEFAULT NULL,
  `shipping_type_duration` int(11) DEFAULT NULL,
  `shipping_type_duration_notation` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `shipping_types`;
INSERT INTO `shipping_types` (`shipping_type_id`, `shipping_type_name`, `shipping_type_description`, `shipping_type_duration`, `shipping_type_duration_notation`) VALUES
(1, 'междугородняя', 'Доставка между городами страны', 2, 'дни'),
(2, 'внутригородняя', 'Доставка по городу', 3, 'часы'),
(3, 'областная', 'Доставка по области', 1, 'дни'),
(4, 'междугосударственная', 'Доставка в другую  страну', 1, 'недели');


ALTER TABLE `accounts`
  ADD PRIMARY KEY (`account_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `account_type` (`account_type`),
  ADD KEY `account_owner` (`account_owner`);

ALTER TABLE `account_types`
  ADD PRIMARY KEY (`account_type_id`);

ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `product` (`product`),
  ADD KEY `options` (`options`),
  ADD KEY `account` (`account`);

ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

ALTER TABLE `currencies`
  ADD PRIMARY KEY (`currency_id`);

ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `customer` (`customer`),
  ADD KEY `order_status` (`order_status`),
  ADD KEY `order_shipping` (`shipping`),
  ADD KEY `order_payment` (`payment`);

ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_detail_id`),
  ADD KEY `order_details_product` (`product`),
  ADD KEY `order_details_options` (`options`),
  ADD KEY `order_id` (`order_id`);

ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`order_status_id`);

ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `payment_currency` (`payment_currency`),
  ADD KEY `payment_type` (`payment_type`);

ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`payment_type_id`);

ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `product_material` (`product_material`),
  ADD KEY `product_provider` (`product_provider`);

ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`product_category_id`),
  ADD KEY `category` (`category`),
  ADD KEY `category_product` (`product`);

ALTER TABLE `product_colors`
  ADD PRIMARY KEY (`product_color_id`);

ALTER TABLE `product_materials`
  ADD PRIMARY KEY (`material_id`);

ALTER TABLE `product_options`
  ADD PRIMARY KEY (`option_id`),
  ADD KEY `product_color` (`color`),
  ADD KEY `product_size` (`size`),
  ADD KEY `product_type` (`type`),
  ADD KEY `option_product` (`product`);

ALTER TABLE `product_providers`
  ADD PRIMARY KEY (`product_provider_id`);

ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`product_size_id`);

ALTER TABLE `product_types`
  ADD PRIMARY KEY (`product_type_id`);

ALTER TABLE `shippers`
  ADD PRIMARY KEY (`shipper_id`);

ALTER TABLE `shipping`
  ADD PRIMARY KEY (`shipping_id`),
  ADD KEY `shipper` (`shipper`),
  ADD KEY `shipping_type` (`shipping_type`);

ALTER TABLE `shipping_types`
  ADD PRIMARY KEY (`shipping_type_id`);


ALTER TABLE `accounts`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `account_types`
  MODIFY `account_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `currencies`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_details`
  MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

ALTER TABLE `order_statuses`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `payment_types`
  MODIFY `payment_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `product_categories`
  MODIFY `product_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `product_colors`
  MODIFY `product_color_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `product_materials`
  MODIFY `material_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `product_options`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `product_providers`
  MODIFY `product_provider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `product_sizes`
  MODIFY `product_size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `product_types`
  MODIFY `product_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `shippers`
  MODIFY `shipper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `shipping`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `shipping_types`
  MODIFY `shipping_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;


ALTER TABLE `accounts`
  ADD CONSTRAINT `account_owner` FOREIGN KEY (`account_owner`) REFERENCES `customers` (`customer_id`),
  ADD CONSTRAINT `account_type` FOREIGN KEY (`account_type`) REFERENCES `account_types` (`account_type_id`);

ALTER TABLE `cart`
  ADD CONSTRAINT `account` FOREIGN KEY (`account`) REFERENCES `accounts` (`account_id`),
  ADD CONSTRAINT `cart_option` FOREIGN KEY (`options`) REFERENCES `product_options` (`option_id`),
  ADD CONSTRAINT `cart_product` FOREIGN KEY (`product`) REFERENCES `products` (`product_id`);

ALTER TABLE `orders`
  ADD CONSTRAINT `customer` FOREIGN KEY (`customer`) REFERENCES `customers` (`customer_id`),
  ADD CONSTRAINT `order_payment` FOREIGN KEY (`payment`) REFERENCES `payment` (`payment_id`),
  ADD CONSTRAINT `order_shipping` FOREIGN KEY (`shipping`) REFERENCES `shipping` (`shipping_id`),
  ADD CONSTRAINT `order_status` FOREIGN KEY (`order_status`) REFERENCES `order_statuses` (`order_status_id`);

ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_options` FOREIGN KEY (`options`) REFERENCES `product_options` (`option_id`),
  ADD CONSTRAINT `order_details_product` FOREIGN KEY (`product`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `payment`
  ADD CONSTRAINT `payment_currency` FOREIGN KEY (`payment_currency`) REFERENCES `currencies` (`currency_id`),
  ADD CONSTRAINT `payment_type` FOREIGN KEY (`payment_type`) REFERENCES `payment_types` (`payment_type_id`);

ALTER TABLE `products`
  ADD CONSTRAINT `product_material` FOREIGN KEY (`product_material`) REFERENCES `product_materials` (`material_id`),
  ADD CONSTRAINT `product_provider` FOREIGN KEY (`product_provider`) REFERENCES `product_providers` (`product_provider_id`);

ALTER TABLE `product_categories`
  ADD CONSTRAINT `category` FOREIGN KEY (`category`) REFERENCES `categories` (`category_id`),
  ADD CONSTRAINT `category_product` FOREIGN KEY (`product`) REFERENCES `products` (`product_id`);

ALTER TABLE `product_options`
  ADD CONSTRAINT `option_product` FOREIGN KEY (`product`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `product_color` FOREIGN KEY (`color`) REFERENCES `product_colors` (`product_color_id`),
  ADD CONSTRAINT `product_size` FOREIGN KEY (`size`) REFERENCES `product_sizes` (`product_size_id`),
  ADD CONSTRAINT `product_type` FOREIGN KEY (`type`) REFERENCES `product_types` (`product_type_id`);

ALTER TABLE `shipping`
  ADD CONSTRAINT `shipper` FOREIGN KEY (`shipper`) REFERENCES `shippers` (`shipper_id`),
  ADD CONSTRAINT `shipping_type` FOREIGN KEY (`shipping_type`) REFERENCES `shipping_types` (`shipping_type_id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
