const { UsersModel } = require('../models');

const notAuthorized = (req, res, next) => {
    if (req.path === "/login" || req.path === "/register" || req.path === "/auth") {
        next();
    } else {
        res.status(401).json({
            status: 401,
            success: false,
            message: "Not authorized"
        });
    }
}

const authMiddleware = (req, res, next) => {
    let authToken = 'authToken' in req.cookies ? req.cookies.authToken : "";
    if (authToken !== "") {
        UsersModel.getAuthUser(authToken, (results) => {
            if (results.success) {
                next();
            } else {
                notAuthorized(req, res, next);
            }
        });
    } else {
        notAuthorized(req, res, next);
    }
};

module.exports = authMiddleware;