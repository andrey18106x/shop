const productsRouter = require('express').Router();
const { ProductsController } = require('../../../controllers');


productsRouter.get("/", ProductsController.getProducts);
productsRouter.get("/:id", ProductsController.getProduct);

module.exports = productsRouter;
