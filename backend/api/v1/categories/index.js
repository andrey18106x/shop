const categoriesRouter = require("express").Router();
const { CategoriesController } = require("../../../controllers/");


categoriesRouter.get("/", CategoriesController.getCategories);
categoriesRouter.get("/:id", CategoriesController.getCategory);
categoriesRouter.get("/:id/products", CategoriesController.getCategoryProducts);

module.exports = categoriesRouter;
