const v1ApiRouter = require('express').Router();

const categoriesRouter = require('./categories');
const productsRouter = require('./products');
const usersRouter = require('./users');


v1ApiRouter.use("/users", usersRouter);
v1ApiRouter.use("/categories", categoriesRouter);
v1ApiRouter.use("/products", productsRouter);


module.exports = v1ApiRouter;
