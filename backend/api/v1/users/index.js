const usersRouter = require('express').Router();
const { UsersController, OrdersController, CartsController } = require('../../../controllers/');
const { authMiddleware } = require('../../../middlewares/');


/** User Default Operations 
 * GET "/auth" - Most used route for authentication
 * POST "/login" - Authorize User and set authToken
 * POST "/logout" - Logout User and revoke authToken
 * POST "/register" - Create a new User (Account and Customer records), 
 * authorize and set authToken
*/

usersRouter.get("/auth", UsersController.getAuthUser);
usersRouter.post("/login", UsersController.login);
usersRouter.post("/logout", UsersController.logout);
usersRouter.post("/register", UsersController.register);

usersRouter.use("/:id", authMiddleware);
usersRouter.get("/:id", UsersController.getUser);

/** *********************** */


/** Orders Routes:
 * GET "/:id/orders" - Get all User Orders (:id - user's account_id)
 * GET "/:id/orders/:orderId" - Get User Order Details
 * POST "/:id/orders" - Create new Order
 * PUT "/:id/orders/:orderId/reject" - Reject order (specified order status)
 */

usersRouter.get("/:id/orders", OrdersController.getUserOrders);
usersRouter.get("/:id/orders/:orderId", OrdersController.getUserOrderDetails);
usersRouter.post("/:id/orders", OrdersController.addNewOrder);
usersRouter.put("/:id/orders/:orderId/reject", OrdersController.rejectOrder);

/** ************* */


/** Cart Routes 
 * GET "/:id/cart" - Get all Products in User cart (:id - user's account_id)
 * GET "/:id/cart/:cartId" - Get cart Product Details
 * POST "/:id/cart" - Add Product to User cart
 * PUT "/:id/cart/:cartId" - Update User cart Product
 * DELETE "/:id/cart/:cartId" - Delete Product from User cart
 * DELETE "/:id/cart/checkout" - Checkout cart
*/

usersRouter.get("/:id/cart", CartsController.getUserCart);
usersRouter.get("/:id/cart/:cartId", CartsController.getCartDetails);
usersRouter.post("/:id/cart", CartsController.addProductToCart);
usersRouter.put("/:id/cart/:cartId", CartsController.updateCartProduct);
usersRouter.delete("/:id/cart/checkout", CartsController.checkoutCart);
usersRouter.delete("/:id/cart/:cartId", CartsController.removeCartProduct);

/** *********** */


module.exports = usersRouter;
