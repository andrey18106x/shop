const apiRouter = require('express').Router();
const v1ApiRouter = require('./v1');

apiRouter.use("/v1", v1ApiRouter);

module.exports = apiRouter;
