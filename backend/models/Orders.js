const database = require("./database");

class OrdersModel {

    /**
     * Returns list of all User orders
     * 
     * @param {Number} customer_id 
     * @param {Function} callback 
     */
    getUserOrders(customer_id, callback) {
        let ordersListQuery = `SELECT orders.order_id, customers.customer_id, customers.first_name, 
        customers.last_name, customers.medium_name, customers.phone, orders.order_date, 
        orders.total_price, payment.payment_name, payment_types.payment_type_name, 
        currencies.currency_name, currencies.currency_code, shipping.shipping_id, 
        shippers.shipper_name, shipping_types.shipping_type_name, shipping_types.shipping_type_description, 
        shipping_types.shipping_type_duration, shipping_types.shipping_type_duration_notation, 
        shipping.shipping_price, order_statuses.order_status_id, order_statuses.order_status_name, 
        Count(order_details.order_id) AS order_products_count
        FROM ((shipping_types INNER JOIN (shippers INNER JOIN shipping 
            ON shippers.shipper_id = shipping.shipper) 
            ON shipping_types.shipping_type_id = shipping.shipping_type) 
        INNER JOIN ((payment_types INNER JOIN (currencies INNER JOIN payment 
            ON currencies.currency_id = payment.payment_currency) 
            ON payment_types.payment_type_id = payment.payment_type) 
        INNER JOIN (order_statuses INNER JOIN (customers INNER JOIN orders 
            ON customers.customer_id = orders.customer) 
            ON order_statuses.order_status_id = orders.order_status) 
            ON payment.payment_id = orders.payment) 
            ON shipping.shipping_id = orders.shipping) 
        INNER JOIN order_details ON orders.order_id = order_details.order_id
        GROUP BY orders.order_id, customers.customer_id, customers.first_name, customers.last_name, 
        customers.medium_name, customers.phone, orders.order_date, orders.total_price, 
        payment.payment_name, payment_types.payment_type_name, currencies.currency_name, 
        currencies.currency_code, shipping.shipping_id, shippers.shipper_name, 
        shipping_types.shipping_type_name, shipping_types.shipping_type_description, 
        shipping_types.shipping_type_duration, shipping_types.shipping_type_duration_notation, 
        shipping.shipping_price, order_statuses.order_status_id, order_statuses.order_status_name
        HAVING customers.customer_id=?;`;
        database.executeQuery(ordersListQuery, [customer_id], (results) => {
            callback(results);
        });
    }

    /**
     * Returns Order Details
     * 
     * @param {Number} customer_id 
     * @param {Number} order_id 
     * @param {Function} callback 
     */
    getUserOrderDetails(customer_id, order_id, callback) {
        let orderDetailsQuery = `SELECT order_details.order_detail_id, products.product_id, 
        products.product_serial_code, products.product_name, product_providers.product_provider_name, 
        products.product_price, products.product_description, product_materials.material_name, 
        products.product_image, order_details.count, product_options.option_id, 
        product_types.product_type_name, product_colors.product_color_name, product_sizes.product_size_name, 
        product_options.available
        FROM orders INNER JOIN (product_colors INNER JOIN (product_sizes INNER JOIN (product_types INNER JOIN 
        (product_materials INNER JOIN (product_providers INNER JOIN (products INNER JOIN 
        (product_options INNER JOIN order_details ON product_options.option_id = order_details.options) 
        ON (products.product_id = product_options.product) AND (products.product_id = order_details.product)) 
        ON product_providers.product_provider_id = products.product_provider) 
        ON product_materials.material_id = products.product_material) 
        ON product_types.product_type_id = product_options.type) 
        ON product_sizes.product_size_id = product_options.size) 
        ON product_colors.product_color_id = product_options.color) 
        ON orders.order_id = order_details.order_id
        WHERE orders.order_id=? AND orders.customer=?;`;
        database.executeQuery(orderDetailsQuery, [order_id, customer_id], (results) => {
            callback(results);
        });
    }

    /**
     * Creates a new record in User Orders
     * 
     * @param {Number} customer_id 
     * @param {Object} orderData 
     * @param {Function} callback 
     */
    addNewOrder(orderData, orderProducts, callback) {
        let newOrderQuery = `INSERT INTO orders (customer, order_date, total_price, payment, shipping, order_status)
        VALUES (?, ?, ?, ?, ?, ?)`;
        let newOrderProductQuery = `INSERT INTO order_details (order_id, product, count, options) 
        VALUES (?, ?, ?, ?)`;
        database.executeQuery(newOrderQuery, Object.values(orderData), (results) => {
            if (results.success) {
                let newOrderId = results.message.insertId;
                let success = true;
                orderProducts.forEach((orderProduct) => {
                    let orderDetails = [newOrderId, orderProduct.product_id, orderProduct.count, orderProduct.options];
                    database.executeQuery(newOrderProductQuery, orderDetails, (orderProductAdded) => {
                        success &= orderProductAdded.success;
                    });
                });
                results.success = success;
                callback(results);
            }
        })
    }

    rejectOrder(order_id, callback) {
        let rejectOrderQuery = `UPDATE orders SET order_status=6 WHERE order_id=?`;
        database.executeQuery(rejectOrderQuery, [order_id], (results) => {
            callback(results);
        });
    }

}

module.exports = new OrdersModel();