const database = require("./database");

class UsersModel {

    /**
     * Returns User information by account_id
     * 
     * @param {Number} account_id 
     * @param {Function} callback 
     */
    getUser(account_id, callback) {
        let userQuery = `SELECT accounts.account_id, account_types.account_type_name, 
        accounts.account_date_added, accounts.email, customers.customer_id, 
        customers.first_name, customers.last_name, customers.medium_name, customers.gender, 
        customers.age, customers.birthday, customers.date_added, customers.country, 
        customers.address, customers.zip_code, customers.phone
        FROM account_types INNER JOIN (customers INNER JOIN accounts ON customers.customer_id = accounts.account_owner) 
        ON account_types.account_type_id = accounts.account_type
        WHERE (((accounts.account_id)=?));`;
        database.executeQuery(userQuery, [account_id], (results) => {
            if (!results.message.length > 0) {
                results.success = false;
            } else {
                results.message = results.message[0];
            }
            callback(results);
        });
    }

    /**
     * Authorize user by login and password
     * 
     * @param {String} email 
     * @param {String} password 
     * @param {Function} callback 
     */
    login(email, password, callback) {
        let isLoginValidQuery = `SELECT accounts.account_id, accounts.email, accounts.password 
        FROM accounts WHERE accounts.email=? AND accounts.password=?`;
        database.executeQuery(isLoginValidQuery, [email, password], (results) => {
            if (results.message.length === 0) {
                results.success = false;
            } else {
                results.message = results.message[0];
            }
            callback(results);
        });
    }

    /**
     * Register new user. (2 tables affected: accounts and customers)
     * 
     * @param {Object} registerCustomerData 
     * @param {Object} registerAccountData
     * @param {Function} callback 
     */
    register(registerCustomerData, registerAccountData, callback) {
        let registerCustomerQuery = `INSERT INTO customers 
        (first_name, last_name, medium_name, gender, age, birthday, date_added, country, address, zip_code, phone)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
        let registerAccountQuery = `INSERT INTO accounts 
        (account_type, account_date_added, account_owner, email, password, token)
        VALUES (?, ?, ?, ?, ?, ?)`;
        database.executeQuery(registerCustomerQuery, Object.values(registerCustomerData), (registerCustomerResults) => {
            if (registerCustomerResults.success) {
                registerAccountData.account_owner = registerCustomerResults.message.insertId;
                database.executeQuery(registerAccountQuery, Object.values(registerAccountData), (results) => {
                    callback(results);
                });
            }
        });
    }

    /**
     * Returns Authorized Account info by AuthToken
     * 
     * @param {String} token 
     * @param {Function} callback 
     */
    getAuthUser(token, callback) {
        if (token !== "" && token !== undefined) {
            let authUserQuery = `SELECT account_id, account_type, account_owner, email FROM accounts WHERE accounts.token=?`;
            database.executeQuery(authUserQuery, [token], (results) => {
                if (results.message.length === 0) {
                    results.success = false;
                } else {
                    results.message = results.message[0];
                }
                callback(results);
            });
        }
    }

    /**
     * Setts the Auth Token to authorized account_id
     * 
     * @param {String} token 
     * @param {Number} account_id 
     * @param {Function} callback 
     */
    setToken(token, account_id, callback) {
        let setTokenQuery = `UPDATE accounts SET token=? WHERE account_id=?`;
        database.executeQuery(setTokenQuery, [token, account_id], (results) => {
            callback(results);
        });
    }

    /**
     * Revokes user auth token (when user logging out)
     * 
     * @param {String} token 
     * @param {Function} callback 
     */
    revokeToken(token, callback) {
        let revokeTokenQuery = `UPDATE accounts SET token=NULL WHERE token=?`;
        database.executeQuery(revokeTokenQuery, [token], (results) => {
            console.log(results);
            callback(results);
        });
    }

}

module.exports = new UsersModel();