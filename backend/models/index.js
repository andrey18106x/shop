const database = require('./database');
const ProductsModel = require('./Products');
const UsersModel = require('./Users');
const OrdersModel = require('./Orders');
const CategoriesModel = require('./Categories');
const CartsModel = require('./Carts');


module.exports = {
    database,
    ProductsModel,
    UsersModel,
    OrdersModel,
    CategoriesModel,
    CartsModel
};