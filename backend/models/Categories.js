const database = require("./database");

class CategoriesModel {

    /**
     * Returns list of all Categories
     * 
     * @param {Function} callback 
     */
    getCategories(callback) {
        database.executeQuery("SELECT * FROM `categories`", (results) => {
            callback(results);
        });
    }

    /**
     * Returns Category short info
     * 
     * @param {Number} category_id 
     * @param {Function} callback 
     */
    getCategory(category_id, callback) {
        let categoryInfoQuery = `SELECT categories.category_id, categories.category_name, 
        categories.category_description, Count(product_categories.product) AS category_products_count
        FROM categories LEFT JOIN product_categories 
        ON categories.category_id = product_categories.category
        WHERE categories.category_id=?
        GROUP BY categories.category_id, categories.category_name, categories.category_description;`;
        database.executeQuery(categoryInfoQuery, [category_id], (results) => {
            if (results.message.length === 0) {
                results.success = false;
            } else {
                results.message = results.message[0];
            };
            callback(results);
        });
    }

    /**
     * Returns list of Products in specific Category
     * 
     * @param {Number} category_id 
     * @param {Function} callback 
     */
    getCategoryProducts(category_id, callback) {
        // TODO: Add Limit and Offset for pagination
        let categoryProductQuery = `SELECT products.product_id, products.product_serial_code, 
        products.product_name, products.product_image
        FROM ((product_providers INNER JOIN (product_materials INNER JOIN products 
        ON product_materials.material_id = products.product_material) 
        ON product_providers.product_provider_id = products.product_provider) 
        INNER JOIN (categories INNER JOIN product_categories 
        ON categories.category_id = product_categories.category) 
        ON products.product_id = product_categories.product) 
        INNER JOIN product_options ON products.product_id = product_options.product
        WHERE (((categories.category_id)=?) AND ((product_options.available)>0))
        GROUP BY products.product_id, products.product_serial_code, products.product_name, products.product_image;`;
        database.executeQuery(categoryProductQuery, [category_id], (results) => {
            callback(results);
        });
    }

}

module.exports = new CategoriesModel();