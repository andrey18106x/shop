const database = require('./database');

class ProductModel {

    /**
     * Returns list of all Products with basic information
     * 
     * @param {Function} callback
     */
    getProducts(callback) {
        database.executeQuery("SELECT * FROM `products`", (results) => {
            callback(results);
        });
    }

    /**
     * Get information about curtain Product.
     * If `detailed = true` - returns list with all Product Options
     * 
     * @param {Number} product_id 
     * @param {boolean} detailed 
     * @param {Function} callback 
     */
    getProduct(product_id, detailed, callback) {
        let productInfoQuery = `SELECT products.product_id, products.product_serial_code, 
        products.product_name, product_providers.product_provider_name, products.product_price, 
        products.product_description, product_materials.material_name, 
        product_types.product_type_name, product_colors.product_color_name, 
        product_sizes.product_size_name, product_options.available, products.product_image, 
        product_options.option_id
        FROM (product_providers INNER JOIN (product_materials INNER JOIN products 
        ON product_materials.material_id = products.product_material) 
        ON product_providers.product_provider_id = products.product_provider) 
        INNER JOIN (product_types INNER JOIN (product_sizes INNER JOIN 
        (product_colors INNER JOIN product_options 
        ON product_colors.product_color_id = product_options.color) 
        ON product_sizes.product_size_id = product_options.size) 
        ON product_types.product_type_id = product_options.type) 
        ON products.product_id = product_options.product
        WHERE products.product_id=?;`;
        if (!detailed) {
            productInfoQuery = "SELECT * FROM products WHERE product_id=?";
        }
        database.executeQuery(productInfoQuery, [product_id], (results) => {
            if (!results.message.length > 0) {
                results.success = false;
            }
            callback(results);
        });
    }

}

module.exports = new ProductModel();