const mysql = require("mysql2");

class Database {

    constructor() {
        this.conn = null;
        this.connectionLimit = process.env.DATABASE_CONN_LIMIT;
        this.host = process.env.DATABASE_HOST;
        this.port = process.env.DATABASE_PORT;
        this.user = process.env.DATABASE_USER;
        this.password = process.env.DATABASE_PASSWORD;
        this.database = process.env.DATABASE_NAME;
    }

    createConnection() {
        this.conn = mysql.createPool({
            connectionLimit: this.connectionLimit,
            host: this.host,
            port: this.port,
            user: this.user,
            password: this.password,
            database: this.database,
        });
    }

    validateConnection(callback) {
        this.executeQuery("SHOW TABLES", (result) => {
            callback(result.message.length === Number(process.env.DATABASE_TABLES_COUNT));
        });
    }

    executeQuery(...args) {
        let query = args[0];
        let params = null;
        let callback = null;

        if (args.length === 2) {
            callback = args[1];
            params = [];
        } else if (args.length === 3) {
            params = args[1];
            callback = args[2];
        } else {
            throw new Error("Invalid arguments count");
        }

        this.conn.execute(query, params, (error, result) => {
            if (error) {
                callback(this._error("Mysql query error\n" + error));
            } else {
                callback(this._success(result));
            }
        });
    }

    _success(msg) {
        return {
            success: true,
            message: msg,
        };
    }

    _error(msg) {
        return {
            success: false,
            message: msg,
        };
    }

    closeConnection() {
        this.conn.end();
    }
}

module.exports = new Database();
