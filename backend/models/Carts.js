const database = require('./database');


class CartsModel {

    /**
     * Returns list of Products (with details) in User Cart
     * 
     * @param {Number} account_id 
     * @param {Function} callback 
     */
    getUserCart(account_id, callback) {
        let getCartQuery = `SELECT cart.cart_id, cart.account, cart.count, cart.options, 
        products.product_id, products.product_name, products.product_price, products.product_image, 
        product_sizes.product_size_name
        FROM products INNER JOIN (product_sizes INNER JOIN 
        (product_options INNER JOIN cart ON product_options.option_id = cart.options) 
        ON product_sizes.product_size_id = product_options.size) 
        ON (products.product_id = product_options.product) AND (products.product_id = cart.product)
        WHERE cart.account=?;`;
        database.executeQuery(getCartQuery, [account_id], (results) => {
            callback(results);
        });
    }


    /**
     * Returns Cart Product Details
     * 
     * @param {Number} account_id 
     * @param {Number} cart_id 
     * @param {Function} callback 
     */
    getCartDetails(account_id, cart_id, callback) {
        let cartProductDetailsQuery = `SELECT cart.cart_id, cart.account, products.product_id, 
        products.product_serial_code, products.product_name, product_providers.product_provider_name, 
        products.product_price, products.product_description, product_materials.material_name, 
        products.product_image, cart.count, product_options.option_id, product_types.product_type_name, 
        product_colors.product_color_name, product_sizes.product_size_name, product_options.available
        FROM product_colors INNER JOIN (product_sizes INNER JOIN (product_types INNER JOIN 
        (product_materials INNER JOIN (product_providers INNER JOIN 
        (product_options INNER JOIN (products INNER JOIN cart ON products.product_id = cart.product) 
        ON (products.product_id = product_options.product) AND (product_options.option_id = cart.options)) 
        ON product_providers.product_provider_id = products.product_provider) 
        ON product_materials.material_id = products.product_material) 
        ON product_types.product_type_id = product_options.type) 
        ON product_sizes.product_size_id = product_options.size) 
        ON product_colors.product_color_id = product_options.color
        WHERE cart.cart_id=? AND cart.account=?`;
        database.executeQuery(cartProductDetailsQuery, [cart_id, account_id], (results) => {
            callback(results);
        });
    }

    /**
     * Add Product to User Cart
     * 
     * @param {Number} account_id 
     * @param {Object} cartData 
     * @param {Function} callback 
     */
    addProductToCart(cartData, callback) {
        let addProductToCartQuery = `INSERT INTO cart (account, product, count, options) VALUES (?, ?, ?, ?);`;
        database.executeQuery(addProductToCartQuery, Object.values(cartData), (results) => {
            callback(results);
        });
    }

    /**
     * Update user cart Product Data
     * 
     * @param {Number} account_id 
     * @param {Number} cart_id 
     * @param {Object} cartData 
     * @param {Function} callback 
     */
    updateCartProduct(account_id, cart_id, cartData, callback) {
        let updateCartProductQuery = `UPDATE cart SET count=?, options=? WHERE cart_id=? AND account=?`;
        database.executeQuery(updateCartProductQuery, Object.values(cartData).concat([cart_id, account_id]), (results) => {
            callback(results);
        });
    }

    /**
     * Delete Product from User cart
     * 
     * @param {Number} account_id 
     * @param {Numbar} cart_id 
     * @param {Function} callback 
     */
    removeCartProduct(account_id, cart_id, callback) {
        let removeCartProductQuery = `DELETE FROM cart WHERE cart_id=? AND account=?`;
        database.executeQuery(removeCartProductQuery, [cart_id, account_id], (results) => {
            callback(results);
        });
    }

    /**
     * Clear User cart after a new Order succesfully created
     * 
     * @param {Number} account_id 
     * @param {Function} callback 
     */
    checkoutCart(account_id, callback) {
        let checkoutCartQuery = `DELETE FROM cart WHERE account=?;`;
        database.executeQuery(checkoutCartQuery, [account_id], (results) => {
            callback(results);
        });
    }

}


module.exports = new CartsModel();
