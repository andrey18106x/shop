# Angular shop backend

Angular shop backend for communicating with the database via REST API.

## How to start

Before first start, you should configure `.env` file to run the database server (e.g. OpenServer) and then install backend server dependencies:

```sh
npm install
```

### Configure database

Import the SQL dump `ng_shop_db.sql` on your local server (contains automatic database creation if not exists).

### Configuring server .env file for development

Here is the explanation of the server `.env` file.

```
HOST=localhost (backend server host)
PORT=3030 (backend server port)

DATABASE_CONN_LIMIT=10 (connection limit to the database server)
DATABASE_HOST=localhost (database server host)
DATABASE_PORT=3307 (database server port)
DATABASE_NAME=ng_shop_db (database name)
DATABASE_USER=default_user (database user)
DATABASE_PASSWORD=232731 (database password)

DATABASE_TABLES_COUNT=10 (database schema tables count)

CORS_ALLOWED_ORIGIN=http://localhost:4200 (frontend host)
CORS_CREDENTIALS=true (for auth from frontend)

```

### Configuring server .env.production for production

For using production version of Angular Shop application the backend server should be configured on subdomain because of cookies security policy.

```
HOST=api.shop.com.ua (local domain for production backend, 127.0.0.1 api.shop.com.ua in system hosts file)
PORT=3030

DATABASE_CONN_LIMIT=10
DATABASE_HOST=localhost
DATABASE_PORT=3307
DATABASE_NAME=ng_shop_db
DATABASE_USER=default_user
DATABASE_PASSWORD=232731

DATABASE_TABLES_COUNT=22

CORS_ALLOWED_ORIGIN=http://shop.com.ua (local web server domain, e.g. Open Server)
CORS_CREDENTIALS=true
```

### Starting backend server

After database server start, you can run the backend server. In command shell start the server from the backend root directory.

Production: 

```sh
npm run server
```

Development:

```sh
npm run server:dev
```


If all looks good, you'll see such message:

```
Database successfully connected
Server is running at http://localhost:3030
```

or if in production:

```
Database successfully connected
Server is running at http://api.shop.com.ua:3030
```