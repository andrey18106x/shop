const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const cors = require("cors");

const { database } = require('./models');
const apiRouter = require("./api");

const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT || 3030;
const PUBLIC = path.join(__dirname, "public");

const corsOptions = {
    origin: [process.env.CORS_ALLOWED_ORIGIN],
    credentials: Boolean(process.env.CORS_CREDENTIALS)
};

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(PUBLIC));
app.use(cors(corsOptions));


app.use("/api", apiRouter);

app.use((req, res) => {
    res.status(404).json({
        status: 404,
        message: "Not found"
    });
});


database.createConnection();
database.validateConnection((connected) => {
    if (connected) {
        process.on('exit', database.closeConnection);
        app.listen(PORT, HOST, () => {
            console.info("Database successfully connected");
            console.info(`Server is running at http://${HOST}:${PORT}`);
        });
    } else {
        console.error("Database tables count not valid");
        console.error("Can't start server without valid database connection");
    }
});
