const { OrdersModel } = require('../models/');


class OrdersController {

    getUserOrders(req, res) {
        let customer_id = req.params.id;
        OrdersModel.getUserOrders(customer_id, (results) => {
            if (results.success) {
                return res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                return res.status(404).json({
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    getUserOrderDetails(req, res) {
        let customer_id = req.params.id;
        let order_id = req.params.orderId;
        OrdersModel.getUserOrderDetails(customer_id, order_id, (results) => {
            if (results.success) {
                return res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                return res.status(404).json({
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    addNewOrder(req, res) {
        let dateAdded = new Date().toLocaleDateString().split('.').reverse().join('-');
        let orderProducts = req.body.products;

        let orderData = {
            customer: req.body.customer,
            order_date: dateAdded,
            total_price: req.body.total_price,
            payment: req.body.payment,
            shipping: req.body.shipping,
            order_status: req.body.order_status
        }

        OrdersModel.addNewOrder(orderData, orderProducts, (results) => {
            if (results.success) {
                return res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                return res.status(404).json({
                    success: results.success,
                    message: results.message
                });
            }
        })
    }

    rejectOrder(req, res) {
        let order_id = req.params.orderId;
        OrdersModel.rejectOrder(order_id, (results) => {
            if (results.success) {
                return res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                return res.status(404).json({
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

}

module.exports = new OrdersController();