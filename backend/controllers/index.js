const ProductsController = require('./Products');
const UsersController = require('./Users');
const OrdersController = require('./Orders');
const CategoriesController = require('./Categories');
const CartsController = require('./Carts');


module.exports = {
    ProductsController,
    UsersController,
    OrdersController,
    CategoriesController,
    CartsController
};