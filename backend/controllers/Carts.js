const { CartsModel } = require("../models");

class CartsController {

    getUserCart(req, res) {
        let account_id = req.params.id;
        CartsModel.getUserCart(account_id, (results) => {
            if (results.success) {
                return res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                return res.status(404).json({
                    status: 404,
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    getCartDetails(req, res) {
        let account_id = req.params.id;
        let cart_id = req.params.cartId;
        CartsModel.getCartDetails(account_id, cart_id, (results) => {
            if (results.success) {
                return res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                return res.status(404).json({
                    status: 404,
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    addProductToCart(req, res) {
        let account_id = req.params.id;
        let cartData = {
            account: account_id,
            product: req.body.product,
            count: req.body.count,
            options: req.body.options
        }
        CartsModel.addProductToCart(cartData, (results) => {
            if (results.success) {
                res.status(201).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                res.status(404).json({
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    updateCartProduct(req, res) {
        let account_id = req.params.id;
        let cart_id = req.params.cartId;
        let cartData = {
            count: req.body.count,
            options: req.body.options
        }

        CartsModel.updateCartProduct(account_id, cart_id, cartData, (results) => {
            if (results.success) {
                res.status(201).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                res.status(404).json({
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    removeCartProduct(req, res) {
        let account_id = req.params.id;
        let cart_id = req.params.cartId;
        CartsModel.removeCartProduct(account_id, cart_id, (results) => {
            if (results.success) {
                res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                res.status(404).json({
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    checkoutCart(req, res) {
        let account_id = req.params.id;
        CartsModel.checkoutCart(account_id, (results) => {
            if (results.success) {
                res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                res.status(404).json({
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

}

module.exports = new CartsController();