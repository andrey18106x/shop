const { UsersModel } = require('../models');
const md5 = require('md5');


const calculateAge = (date) => {
    let birthday = new Date(date);
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}


class UsersController {

    getUser(req, res) {
        let account_id = req.params.id;
        UsersModel.getUser(account_id, (results) => {
            if (results.success) {
                return res.status(200).json({
                    success: results.success,
                    message: results.message
                });
            } else {
                return res.status(404).json({
                    status: 404,
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    getAuthUser(req, res) {
        let authToken = 'authToken' in req.cookies ? req.cookies.authToken : "";
        if (authToken !== "") {
            UsersModel.getAuthUser(authToken, (results) => {
                if (results.success) {
                    return res.status(200).json({
                        status: 200,
                        success: results.success,
                        message: results.message
                    });
                } else {
                    return res.status(401).json({
                        status: 401,
                        success: results.success
                    });
                }
            });
        } else {
            return res.status(401).json({
                status: 401,
                success: false,
                message: "Not authorized"
            });
        }
    }

    login(req, res) {
        let email = req.body.email;
        let password = req.body.password;
        UsersModel.login(email, password, (results) => {
            if (results.success) {
                let token = md5(new Date().toISOString());
                let account_id = results.message.account_id;
                UsersModel.setToken(token, account_id, (tokenResults) => {
                    if (tokenResults.success) {
                        return res.status(200).json({
                            success: results.success,
                            message: results.message,
                            token: token
                        });
                    } else {
                        return res.status(200).json({
                            status: 401,
                            success: results.success,
                            message: results.message
                        });
                    }
                });
            } else {
                return res.status(200).json({
                    status: 401,
                    success: results.success,
                    message: results.message
                });
            }
        });
    }

    register(req, res) {
        console.log(req.body);
        let dateAdded = new Date().toLocaleDateString().split('.').reverse().join('-');
        let token = md5(new Date().toISOString());

        let registerCustomerData = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            medium_name: req.body.medium_name,
            gender: req.body.gender,
            age: req.body.birthday !== null ? calculateAge(req.body.birthday): null,
            birthday: req.body.birthday,
            date_added: dateAdded,
            country: req.body.country,
            address: req.body.address,
            zip_code: req.body.zip_code,
            phone: req.body.phone
        }

        let registerAccountData = {
            account_type: 1,
            account_date_added: dateAdded,
            account_owner: null,
            email: req.body.email,
            password: req.body.password,
            token: token
        }

        UsersModel.register(registerCustomerData, registerAccountData, (results) => {
            if (results.success) {
                return res.status(201).json({
                    success: results.success,
                    token: token
                });
            } else {
                return res.status(409).json({
                    status: 409,
                    success: results.success,
                    message: results.message
                })
            }
        });
    }

    logout(req, res) {
        let account_id = req.body.account_id;
        UsersModel.setToken(null, account_id, (results) => {
            return res.status(200).json({
                success: results.success,
                message: results.message
            });
        });
    }
}

module.exports = new UsersController();