const { ProductsModel } = require('../models');

const singleResult = (results, res) => {
    if (results.success) {
        return res.status(200).json(results.message);
    } else {
        return res.status(404).json({
            status: 404,
            success: results.success,
            message: results.message
        });
    }
}

class ProductsController {

    getProducts(req, res) {
        ProductsModel.getProducts((results) => {
            singleResult(results, res);
        });
    }

    getProduct(req, res) {
        let product_id = req.params.id;
        let detailed = false;
        if ('detailed' in req.query) {
            detailed = Boolean(req.query.detailed);
        }
        ProductsModel.getProduct(product_id, detailed, (results) => {
            singleResult(results, res);
        });
    }

}

module.exports = new ProductsController();