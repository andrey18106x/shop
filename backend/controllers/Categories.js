const { CategoriesModel } = require("../models");

const singleResult = (results, res) => {
    if (results.success) {
        return res.status(200).json(results.message);
    } else {
        return res.status(404).json({
            status: 404,
            success: results.success,
            message: results.message
        });
    }
}

class CategoriesController {

    getCategories(req, res) {
        CategoriesModel.getCategories((results) => {
            singleResult(results, res);
        });
    }

    getCategory(req, res) {
        let category_id = req.params.id;
        CategoriesModel.getCategory(category_id, (results) => {
            singleResult(results, res);
        });
    }

    getCategoryProducts(req, res) {
        let category_id = req.params.id;
        CategoriesModel.getCategoryProducts(category_id, (results) => {
            singleResult(results, res);
        });
    }

}

module.exports = new CategoriesController();