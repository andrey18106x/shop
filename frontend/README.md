# Shop

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production build

Place the files from `/build` folder with compiled application to some local server (e.g. OpenServer), launch the server and navigate to the application url, configured in your local server (application configured for using `shop.com.ua` domain, so the folder in OpenServer should be named `shop.com.ua`, the backend also uses this domain as allowed for CORS in `.env.production`).

Production build has default environment configured for using backend API at `api.shop.com.ua:3030`.

### Environment variables explanation

```
environment = {
  production: true,
  apiDomain: '.shop.com.ua', // domain for cookies (default: `localhost`)
  apiHost: "http://api.shop.com.ua:3030", // backend host as subdomain
  v1ApiUrl: "/api/v1"
};
```
