import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CategoriesService } from 'src/app/modules/shop/services/categories/categories.service';

@Component({
  selector: 'app-page-home',
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.scss']
})
export class PageHomeComponent implements OnInit {

  title: string = "TShirt Shop | Andrey Borysenko - GFL JS Course";
  loading: boolean = true;

  constructor(private titleService: Title, private categoriesService: CategoriesService) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.categoriesService.categoriesLoaded().subscribe((categoriesLoaded) => {
      this.loading = !categoriesLoaded;
    });
  }

  get categories(): any[] {
    return this.categoriesService.categories;
  }

}
