import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-page-about',
  templateUrl: './page-about.component.html',
  styleUrls: ['./page-about.component.scss']
})
export class PageAboutComponent implements OnInit {

  title: string = "О Нас - TShirt Shop | Andrey Borysenko - GFL JS Course";

  constructor(private titleService: Title) {
    this.titleService.setTitle(this.title);
  }
  ngOnInit(): void {
  }

}
