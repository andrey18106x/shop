import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CookieService } from 'src/app/services/cookie/cookie.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.scss']
})
export class PageLoginComponent implements OnInit {

  title: string = "Авторизация - TShirt Shop | Andrey Borysenko - GFL JS Course";
  apiHost: string = environment.apiHost + environment.v1ApiUrl;
  loginURL: string = this.apiHost + "/login";
  loginFormGroup: any = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  constructor(private titleService: Title, private authService: AuthService, private cookieService: CookieService, private router: Router) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
  }

  onLogin(event: MouseEvent): void {
    event.preventDefault();
    let email = <string>this.loginFormGroup.value.email;
    let password = <string>this.loginFormGroup.value.password;
    if (this.loginFormGroup.status === "VALID") {
      this.authService.login(email, password).subscribe((results: any) => {
        if (results.success) {
          this.cookieService.setCookie("authToken", results.token, {samesite: 'Lax', domain: environment.domain, 'max-age': 3600});
          this.authService.auth().subscribe((authorized) => {
            if (authorized) {
              this.router.navigate(["/"]);
            }
          });
        } else {
          alert("Неверный Email или Пароль. Попробуйте ещё раз.");
        }
      });
    } else {
      alert("Введите корректные данные");
    }
  }

  get loggedIn(): boolean {
    return this.authService.loggedIn;
  }

}
