import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CookieService } from 'src/app/services/cookie/cookie.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-page-register',
  templateUrl: './page-register.component.html',
  styleUrls: ['./page-register.component.scss']
})
export class PageRegisterComponent implements OnInit {

  title: string = "Регистрация - TShirt Shop | Andrey Borysenko - GFL JS Course";
  apiHost: string = environment.apiHost + environment.v1ApiUrl;
  registerURL: string = this.apiHost + "/users/register";
  registerFormGroup: any = new FormGroup({
    first_name: new FormControl('', [Validators.required]),
    last_name: new FormControl('', [Validators.required]),
    medium_name: new FormControl('', [Validators.required]),
    gender: new FormControl(null),
    birthday: new FormControl(null),
    country: new FormControl(null),
    address: new FormControl(null),
    zip_code: new FormControl(null),
    phone: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(13)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    password_confirmation: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  constructor(private authService: AuthService, private titleService: Title, private http: HttpClient, private cookieService: CookieService, private router: Router) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.authService.auth();
  }

  onRegister(event: MouseEvent): void {
    event.preventDefault();
    if (this.registerFormGroup.status === "VALID" 
    && this.registerFormGroup.value.password == this.registerFormGroup.value.password_confirmation) {
      let registerData = this.registerFormGroup.value;
      delete registerData.password_confirmation;
      this.authService.register(registerData).subscribe((results: any) => {
        if (results.success) {
          this.cookieService.setCookie("authToken", results.token, {samesite: 'Lax', domain: environment.domain, 'max-age': 3600});
          this.authService.auth().subscribe((authorized) => {
            if (authorized) {
              this.router.navigate(["/"]);
            }
          });
        } else {
          alert("Ошибка регистрации, попробуйте ещё раз");
        }
      });
    } else {
      alert("Пароль и подтверждение пароля должно быть одинаково!");
    }
  }

  get loggedIn(): boolean {
    return this.authService.loggedIn;
  }

}
