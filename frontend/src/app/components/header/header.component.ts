import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CookieService } from 'src/app/services/cookie/cookie.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService, private cookieService: CookieService, private router: Router) {
  }

  ngOnInit(): void {
    this.authService.auth().subscribe();
  }

  get loggedIn(): boolean {
    return this.authService.loggedIn;
  }

  onLogout(event: MouseEvent): void {
    event.preventDefault();
    this.authService.logout().subscribe((loggedout) => {
      this.cookieService.deleteCookie("authToken");
      this.router.navigate(["/"]);
    });
  }

}
