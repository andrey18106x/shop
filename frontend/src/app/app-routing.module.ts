import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageAboutComponent } from './pages/page-about/page-about.component';
import { PageHomeComponent } from './pages/page-home/page-home.component';
import { PageLoginComponent } from './pages/page-login/page-login.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { PageRegisterComponent } from './pages/page-register/page-register.component';

const routes: Routes = [
  { path: "", component: PageHomeComponent },
  { path: "login", component: PageLoginComponent },
  { path: "register", component: PageRegisterComponent },
  { path: "shop", loadChildren: () => import('./modules/shop/shop.module').then((m) => m.ShopModule) },
  { path: "user", loadChildren: () => import("./modules/user/user.module").then((m) => m.UserModule) },
  { path: "about", component: PageAboutComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
