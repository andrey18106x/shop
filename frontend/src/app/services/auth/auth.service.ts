import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import MD5 from './md5';


interface RegisterData {
  first_name: string;
  last_name: string;
  medium_name: string;
  gender: string;
  birthday: string;
  country: string;
  address: string;
  zip_code: number;
  phone: string;
  email: string;
  password: string;
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiHost: string = environment.apiHost + environment.v1ApiUrl;
  authURL: string = this.apiHost + "/users/auth";
  loggedIn: boolean = false;
  authUser: any = {};

  constructor(private http: HttpClient) { }

  auth(): Observable<boolean> {
    return new Observable((observer) => {
      this.http.get(this.authURL, { withCredentials: true }).subscribe((results: any) => {
        if (results.success) {
          this.loggedIn = results.success;
          this.http.get(`${this.apiHost}/users/${results.message.account_id}`, { withCredentials: true }).subscribe((user: any) => {
            this.loggedIn = this.loggedIn && user.success;
            this.authUser = user.message;
            observer.next(this.loggedIn);
            observer.complete();
          });
        } else {
          observer.next(false);
          observer.complete();
        }
      }, (results) => {
        observer.next(results.error.success);
        observer.complete();
      });
    });
  }

  login(email: string, password: string): Observable<any> {
    return new Observable((observer) => {
      let md5Password = MD5(password);
      this.http.post(`${this.apiHost}/users/login`, { email, password: md5Password }).subscribe((results: any) => {
        if (results.success) {
          observer.next(results);
          observer.complete();
        } else {
          observer.next(results.success);
          observer.complete();
        }
      });
    });
  }

  register(registerData: RegisterData): Observable<any> {
    return new Observable((observer) => {
      registerData.password = MD5(registerData.password);
      this.http.post(`${this.apiHost}/users/register`, registerData).subscribe((results: any) => {
        observer.next(results);
        observer.complete();
      });
    });
  }

  logout(): Observable<boolean> {
    return new Observable((observer) => {
      this.http.post(`${this.apiHost}/users/logout`, { account_id: this.authUser.account_id }).subscribe((results) => {
        this.authUser = {};
        this.loggedIn = false;
        observer.next(true);
      });
    });
  }

}
