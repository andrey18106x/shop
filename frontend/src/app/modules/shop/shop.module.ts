import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopRoutingModule } from './shop-routing.module';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductsService } from './services/products/products.service';
import { ProductPageComponent } from './components/product-page/product-page.component';
import { ShopComponent } from './shop/shop.component';
import { CategoriesListComponent } from './components/categories-list/categories-list.component';
import { CategoryPageComponent } from './components/category-page/category-page.component';
import { PageShopHomeComponent } from './pages/page-shop-home/page-shop-home.component';
import { ShopHeaderComponent } from './components/shop-header/shop-header.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProductsListComponent, 
    ProductPageComponent, 
    ShopComponent, 
    CategoriesListComponent, 
    CategoryPageComponent, 
    PageShopHomeComponent, 
    ShopHeaderComponent
  ],
  imports: [
    CommonModule,
    ShopRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    ProductsService
  ]
})
export class ShopModule { }
