import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products: any[] = [];
  apiHost: string = environment.apiHost + environment.v1ApiUrl;
  productsApiURL: string = this.apiHost + "/products";

  constructor(private http: HttpClient) {
    this.loadProducts().subscribe((products) => {
      this.products = products;
    });
  }

  loadProducts(): Observable<any[]> {
    return new Observable((observer) => {
      this.http.get(this.productsApiURL).subscribe((products) => {
        observer.next(<any[]>products);
        observer.complete();
      });
    });
  }

  productsLoaded(): Observable<boolean> {
    return new Observable((observer) => {
      if (!this.products.length) {
        this.loadProducts().subscribe((products) => {
          this.products = products;
          observer.next(true);
          observer.complete();
        });
      } else {
        observer.next(true);
        observer.complete();
      }
    });
  }

  getProductInfo(productId: number): Observable<any> {
    return new Observable((observer) => {
      this.http.get(this.productsApiURL + "/" + productId + "?detailed=true").subscribe((product_options) => {
        observer.next(product_options);
        observer.complete();
      });
    });
  }

}
