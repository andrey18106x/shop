import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  categories: any[] = [];
  apiUrl: string = environment.apiHost + environment.v1ApiUrl;
  categoriesApiURL: string = this.apiUrl + "/categories";

  constructor(private http: HttpClient) {
    this.loadCategories().subscribe((categories) => {
      this.categories = categories;
    });
  }

  loadCategories(): Observable<any[]> {
    return new Observable((observer) => {
      this.http.get(this.categoriesApiURL).subscribe((categories) => {
        observer.next(<any[]>categories);
        observer.complete();
      });
    });
  }

  loadCategory(category_id: number): Observable<any> {
    return new Observable((observer) => {
      this.http.get(`${this.categoriesApiURL}/${category_id}/`).subscribe((category_info) => {
        observer.next(category_info);
        observer.complete();
      });
    });
  }

  getCategoryProducts(category_id: number): Observable<any[]> {
    return new Observable((observer) => {
      this.http.get(`${this.categoriesApiURL}/${category_id}/products`).subscribe((category_products) => {
        observer.next(<any[]>category_products);
        observer.complete();
      });
    });
  }

  categoriesLoaded(): Observable<boolean> {
    return new Observable((observer) => {
      if (!this.categories.length) {
        this.loadCategories().subscribe((categories) => {
          this.categories = categories;
          observer.next(true);
          observer.complete();
        });
      } else {
        observer.next(true);
        observer.complete();
      }
    });
  }

}
