import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../services/categories/categories.service';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  loading: boolean = true;

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit(): void {
    this.categoriesService.categoriesLoaded().subscribe((categoriesLoaded) => {
      this.loading = !categoriesLoaded;
    });
  }

  get categories(): any[] {
    return this.categoriesService.categories;
  }

}
