import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from 'src/app/modules/user/services/cart/cart.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { environment } from 'src/environments/environment';
import { ProductsService } from '../../services/products/products.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

  environment = environment;
  title: string = "Футболка - TShirt Shop | Andrey Borysenko - GFL JS Course";
  loading: boolean = true;

  productId: number = 0;
  product: any = {};
  product_options: any[] = [];

  productOptionsGroup: any = new FormGroup({
    productOption: new FormControl()
  });

  constructor(private titleService: Title, private authService: AuthService, private route: ActivatedRoute, private productsService: ProductsService, private cartService: CartService, private router: Router) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.route.params.subscribe((param) => {
      this.productId = Number(param.id);
      this.productsService.getProductInfo(this.productId).subscribe((product_options) => {
        if (!('success' in product_options)) {
          this.product = product_options[0];
          this.product_options = product_options;
          this.loading = false;
          this.titleService.setTitle(this.title.replace("Футболка", this.product.product_name));
          this.productOptionsGroup.patchValue({ productOption: this.product.option_id});

          this.productOptionsGroup.get('productOption').valueChanges.subscribe((value: any) => {
            let optionIndex = this.product_options.findIndex((option) => option.option_id == value);
            this.product = this.product_options[optionIndex];
          });
        } else if (!product_options) {
          this.loading = false;
        }
      });
    });
  }

  onProductBuy(): void {
    let newProductData = {
      account: this.authService.authUser.account_id,
      product: this.product.product_id,
      count: 1,
      options: Number(this.productOptionsGroup.get('productOption').value)
    };
    this.cartService.addProduct(newProductData).subscribe((results: any) => {
      if (results.success) {
        this.router.navigate(["/user/cart"]);
      }
    });
  }

}
