import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { ProductsService } from '../../services/products/products.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  environment = environment;
  title: string = "Все футболки - TShirt Shop | Andrey Borysenko - GFL JS Course";
  loading: boolean = true;

  constructor(private productsService: ProductsService, private titleService: Title) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.productsService.productsLoaded().subscribe((productsLoaded) => {
      this.loading = !productsLoaded;
    });
  }

  get products(): any[] {
    return this.productsService.products;
  }

}
