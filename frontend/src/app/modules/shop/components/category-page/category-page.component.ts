import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { CategoriesService } from '../../services/categories/categories.service';


@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.scss']
})
export class CategoryPageComponent implements OnInit {

  environment = environment;
  title: string = "Категории - TShirt Shop | Andrey Borysenko - GFL JS Course";
  category_info: any = {};
  category_products: any[] = [];
  loading: boolean = true;

  constructor(private categoriesService: CategoriesService, private titleService: Title, private route: ActivatedRoute) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      let category_id = params.id;
      this.categoriesService.loadCategory(category_id).subscribe((category_info) => {
        this.category_info = category_info;
        this.categoriesService.getCategoryProducts(category_id).subscribe((category_products) => {
          this.loading = false;
          this.category_products = category_products;
          this.titleService.setTitle(this.title.replace("Категории", this.category_info.category_name));
        });
      });
    });
  }

}
