import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesListComponent } from './components/categories-list/categories-list.component';
import { CategoryPageComponent } from './components/category-page/category-page.component';
import { ProductPageComponent } from './components/product-page/product-page.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { PageShopHomeComponent } from './pages/page-shop-home/page-shop-home.component';
import { ShopComponent } from './shop/shop.component';

const routes: Routes = [
    { path: "", component: ShopComponent,
      children: [
        { path: "", component: PageShopHomeComponent, 
          children: [
            { path: "categories", component: CategoriesListComponent },
            { path: "categories/:id", component: CategoryPageComponent },
            { path: "products", component: ProductsListComponent },
          ]
        },
        { path: "products/:id", component: ProductPageComponent }
      ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
