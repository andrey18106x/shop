import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UserRoutingModule } from './user-routing.module';
import { PageUserHomeComponent } from './pages/page-user-home/page-user-home.component';
import { UserHeaderComponent } from './components/user-header/user-header.component';
import { PageUserProfileComponent } from './pages/page-user-profile/page-user-profile.component';
import { PageUserOrdersComponent } from './pages/page-user-orders/page-user-orders.component';
import { PageUserCartComponent } from './pages/page-user-cart/page-user-cart.component';
import { PageCheckoutComponent } from './pages/page-checkout/page-checkout.component';
import { PageThankYouComponent } from './pages/page-thank-you/page-thank-you.component';


@NgModule({
  declarations: [
    UserComponent, 
    PageUserHomeComponent, 
    UserHeaderComponent, 
    PageUserProfileComponent, 
    PageUserOrdersComponent, 
    PageUserCartComponent,
    PageCheckoutComponent,
    PageThankYouComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
