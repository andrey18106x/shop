import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CartService } from '../../services/cart/cart.service';
import { OrdersService } from '../../services/orders/orders.service';

@Component({
  selector: 'app-page-checkout',
  templateUrl: './page-checkout.component.html',
  styleUrls: ['./page-checkout.component.scss']
})
export class PageCheckoutComponent implements OnInit {

  title: string = "Оформление заказа - TShirt Shop | Andrey Borysenko - GFL JS Course";
  loading: boolean = true;

  constructor(private titleService: Title, private router: Router, private authService: AuthService, private cartService: CartService, private ordersService: OrdersService) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.cartService.loadUserCart(this.authService.authUser.account_id).subscribe((cartProducts: any) => {
      if (cartProducts.success) {
        this.loading = false;
      }
    });
  }

  onSubmitCheckout(): void {
    let orderData = {
      customer: this.authService.authUser.customer_id,
      total_price: this.totalPrice + 50, // temporary added fixed shipping price
      payment: 1, // fixed payment type
      shipping: 1, // fixed shipping type
      order_status: 1, // default approving pending status
      products: this.cartProducts
    }
    this.ordersService.newOrder(orderData, this.authService.authUser.account_id).subscribe((results: any) => {
      if (results.success) {
        this.cartService.checkout(this.authService.authUser.account_id).subscribe((results: any) => {
          this.router.navigate(["/user/thank-you"]);
        });
      } else {
        alert("Ошибка отправки заказа. Попробуйте ещё раз.");
      }
    });
  }

  get cartProducts(): any[] {
    return this.cartService.cartProducts;
  }

  get totalPrice(): number {
    return this.cartService.totalPrice;
  }

}
