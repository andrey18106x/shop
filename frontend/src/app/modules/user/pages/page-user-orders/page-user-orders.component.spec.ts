import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUserOrdersComponent } from './page-user-orders.component';

describe('PageUserOrdersComponent', () => {
  let component: PageUserOrdersComponent;
  let fixture: ComponentFixture<PageUserOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageUserOrdersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageUserOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
