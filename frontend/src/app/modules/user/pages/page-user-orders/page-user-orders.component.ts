import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth/auth.service';
import { OrdersService } from '../../services/orders/orders.service';

@Component({
  selector: 'app-page-user-orders',
  templateUrl: './page-user-orders.component.html',
  styleUrls: ['./page-user-orders.component.scss']
})
export class PageUserOrdersComponent implements OnInit {

  title: string = "Заказы - Личный кабинет - TShirt Shop | Andrey Borysenko - GFL JS Course";
  loading: boolean = true;

  orderDetails: any[] = [];

  constructor(private authService: AuthService, private ordersService: OrdersService, private titleService: Title) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.authService.auth().subscribe((authorized) => {
      if (authorized) {
        this.ordersService.loadUserOrders(this.authService.authUser.customer_id).subscribe((ordersLoaded) => {
          this.loading = !ordersLoaded;
        });
      } else {
        this.loading = false;
      }
    });
  }

  onOrderClick(event: MouseEvent): void {
    let order_id = Number((<HTMLButtonElement>event.target).dataset.orderId);
    if (!(<HTMLButtonElement>event.target).classList.contains("collapsed")) {
      this.ordersService.getOrderDetails(order_id, this.authService.authUser.account_id).subscribe((results: any) => {
        if (results.success) {
          this.orderDetails = results.message;
        }
      });
    }
  }

  onRejectOrder(event: MouseEvent): void {
    let statusId = Number((<HTMLButtonElement>event.target).dataset.orderStatusId);
    let orderId = Number((<HTMLButtonElement>event.target).dataset.orderId);
    if (statusId === 1) {
      this.ordersService.rejectOrder(orderId, this.authService.authUser.account_id).subscribe();
    } else {
      console.log("Can not reject");
    }
  }

  get ordersList(): any[] {
    return this.ordersService.ordersList;
  }

  get loggedIn(): boolean {
    return this.authService.loggedIn;
  }

}
