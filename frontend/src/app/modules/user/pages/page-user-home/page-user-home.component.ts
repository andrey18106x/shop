import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-page-user-home',
  templateUrl: './page-user-home.component.html',
  styleUrls: ['./page-user-home.component.scss']
})
export class PageUserHomeComponent implements OnInit {

  title: string = "Личный кабинет - TShirt Shop | Andrey Borysenko - GFL JS Course";

  constructor(private titleService: Title) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
  }

}
