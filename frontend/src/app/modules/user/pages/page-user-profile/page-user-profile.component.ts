import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-page-user-profile',
  templateUrl: './page-user-profile.component.html',
  styleUrls: ['./page-user-profile.component.scss']
})
export class PageUserProfileComponent implements OnInit {

  title: string = "Профиль - Личный кабинет - TShirt Shop | Andrey Borysenko - GFL JS Course";
  loading: boolean = true;
  profileInfo: any = {};

  constructor(private authService: AuthService, private titleService: Title) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.authService.auth().subscribe((authorized) => {
      this.loading = false;
    });
  }

  get profile(): any {
    return this.authService.authUser;
  }

  get loggedIn(): boolean {
    return this.authService.loggedIn;
  }

}
