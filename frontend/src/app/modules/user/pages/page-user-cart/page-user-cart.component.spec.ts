import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUserCartComponent } from './page-user-cart.component';

describe('PageUserCartComponent', () => {
  let component: PageUserCartComponent;
  let fixture: ComponentFixture<PageUserCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageUserCartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageUserCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
