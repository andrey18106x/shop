import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth/auth.service';
import { environment } from 'src/environments/environment';
import { CartService } from '../../services/cart/cart.service';

@Component({
  selector: 'app-page-user-cart',
  templateUrl: './page-user-cart.component.html',
  styleUrls: ['./page-user-cart.component.scss']
})
export class PageUserCartComponent implements OnInit {

  environment = environment;
  title: string = "Корзина - Личный кабинет - TShirt Shop | Andrey Borysenko - GFL JS Course";
  loading: boolean = true;
  cartProductDetails: any = {};

  constructor(private authService: AuthService, private cartService: CartService, private titleService: Title) {
    this.titleService.setTitle(this.title);
  }

  ngOnInit(): void {
    this.authService.auth().subscribe((authorized) => {
      if (authorized) {
        this.cartService.loadUserCart(this.authService.authUser.account_id).subscribe((cartProducts: any) => {
          this.loading = false;
        });
      } else {
        this.loading = false;
      }
    });
  }

  get loggedIn(): boolean {
    return this.authService.loggedIn;
  }

  get cartProducts(): any[] {
    return this.cartService.cartProducts;
  }

  onCartProductClick(event: MouseEvent): void {
    let cart_id = Number((<HTMLButtonElement>event.target).dataset.cartId);
    if (!(<HTMLButtonElement>event.target).classList.contains("collapsed")) {
      this.cartService.loadCartProductDetails(cart_id, this.authService.authUser.account_id).subscribe((results) => {
        if (results.success) {
          this.cartProductDetails = results.message[0];
        }
      });
    }
  }

  onIncreaseCartProductCount(): void {
    let cartProductData = {
      cart_id: this.cartProductDetails.cart_id,
      account: this.authService.authUser.account_id,
      product: this.cartProductDetails.product_id,
      count: this.cartProductDetails.count + 1,
      options: this.cartProductDetails.option_id
    }
    this.cartService.updateProduct(cartProductData).subscribe((results) => {
      if (results.success) {
        this.cartProductDetails.count += 1;
        this.cartService.loadCartProductDetails(this.cartProductDetails.cart_id, this.authService.authUser.account_id).subscribe((cartProductDetails: any) => {
          if (cartProductDetails.success) {
            this.cartProductDetails = cartProductDetails.message[0];
            (<HTMLButtonElement>document.querySelector("button.accordion-button[data-cart-id='" + cartProductData.cart_id + "']")).click();
          }
        });
      }
    });
  }

  onDecreaseCartProductCount(): void {
    if (this.cartProductDetails.count - 1 > 0) {
      let cartProductData = {
        cart_id: this.cartProductDetails.cart_id,
        account: this.authService.authUser.account_id,
        product: this.cartProductDetails.product_id,
        count: this.cartProductDetails.count - 1,
        options: this.cartProductDetails.option_id
      }
      this.cartService.updateProduct(cartProductData).subscribe((results) => {
        if (results.success) {
          this.cartProductDetails.count -= 1;
          this.cartService.loadCartProductDetails(this.cartProductDetails.cart_id, this.authService.authUser.account_id).subscribe((cartProductDetails: any) => {
            if (cartProductDetails.success) {
              this.cartProductDetails = cartProductDetails.message[0];
              (<HTMLButtonElement>document.querySelector("button.accordion-button[data-cart-id='" + cartProductData.cart_id + "']")).click();
            }
          });
        }
      });
    }
  }

  onDeleteCartProduct(): void {
    let cartProductData = {
      cart_id: this.cartProductDetails.cart_id,
      account: this.authService.authUser.account_id,
      product: this.cartProductDetails.product_id,
      count: this.cartProductDetails.count,
      options: this.cartProductDetails.option_id
    }
    this.cartService.removeProduct(cartProductData).subscribe();
  }

  get cartTotal(): number {
    return this.cartService.totalPrice;
  }

}
