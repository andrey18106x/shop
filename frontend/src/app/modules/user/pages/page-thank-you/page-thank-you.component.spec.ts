import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageThankYouComponent } from './page-thank-you.component';

describe('PageThankYouComponent', () => {
  let component: PageThankYouComponent;
  let fixture: ComponentFixture<PageThankYouComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageThankYouComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageThankYouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
