import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageCheckoutComponent } from './pages/page-checkout/page-checkout.component';
import { PageThankYouComponent } from './pages/page-thank-you/page-thank-you.component';
import { PageUserCartComponent } from './pages/page-user-cart/page-user-cart.component';
import { PageUserHomeComponent } from './pages/page-user-home/page-user-home.component';
import { PageUserOrdersComponent } from './pages/page-user-orders/page-user-orders.component';
import { PageUserProfileComponent } from './pages/page-user-profile/page-user-profile.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  { path: "", component: UserComponent,
    children: [
      { path: "", component: PageUserHomeComponent,
        children: [
          { path: "profile", component: PageUserProfileComponent },
          { path: "orders", component: PageUserOrdersComponent },
          { path: "cart", component: PageUserCartComponent },
          { path: "checkout", component: PageCheckoutComponent },
          { path: "thank-you", component: PageThankYouComponent }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
