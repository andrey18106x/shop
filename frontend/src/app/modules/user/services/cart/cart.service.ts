import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


interface NewCartProduct {
  account: number,
  product: number,
  count: number,
  options: number
}

interface CartData {
  cart_id: number,
  account: number,
  product: number,
  count: number,
  options: number
}


@Injectable({
  providedIn: 'root'
})
export class CartService {

  apiUrl: string = environment.apiHost + environment.v1ApiUrl;
  cartProducts: any[] = [];

  constructor(private http: HttpClient) { }

  loadUserCart(account_id: number): Observable<any[]> {
    return new Observable((observer) => {
      this.http.get(`${this.apiUrl}/users/${account_id}/cart`, { withCredentials: true }).subscribe((cart_products: any) => {
        if (cart_products.success) {
          this.cartProducts = cart_products.message
        }
        observer.next(cart_products);
        observer.complete();
      });
    });
  }

  loadCartProductDetails(cart_id: number, account_id: number): Observable<any> {
    return new Observable((observer) => {
      this.http.get(`${this.apiUrl}/users/${account_id}/cart/${cart_id}`, { withCredentials: true }).subscribe((results: any) => {
        observer.next(results);
        observer.complete();
      });
    });
  }

  addProduct(cartProductData: NewCartProduct): Observable<any> {
    return new Observable((observer) => {
      let cartIndex = this.cartProducts.findIndex((cartProduct) => (cartProduct.product_id === cartProductData.product && cartProduct.options === cartProductData.options));
      if (cartIndex === -1) {
        this.http.post(`${this.apiUrl}/users/${cartProductData.account}/cart`, cartProductData, { withCredentials: true }).subscribe((results: any) => {
          if (results.success) {
            this.loadUserCart(cartProductData.account).subscribe((productLoaded: any) => {
              if (productLoaded.success) {
                observer.next(results);
                observer.complete();
              }
            });
          }
        });
      } else {
        let productData = {
          cart_id: this.cartProducts[cartIndex].cart_id,
          account: this.cartProducts[cartIndex].account,
          product: cartProductData.product,
          count: this.cartProducts[cartIndex].count + 1,
          options: cartProductData.options
        }
        console.log(productData);
        this.updateProduct(productData).subscribe((results) => {
          observer.next(results);
          observer.complete();
        })
      }
    });
  }

  updateProduct(cartProductData: CartData): Observable<any> {
    return new Observable((observer) => {
      this.http.put(`${this.apiUrl}/users/${cartProductData.account}/cart/${cartProductData.cart_id}`, cartProductData, { withCredentials: true }).subscribe((results: any) => {
        if (results.success) {
          this.loadUserCart(cartProductData.account).subscribe((productLoaded: any) => {
            if (productLoaded.success) {
              observer.next(results);
              observer.complete();
            }
          });
        }
      });
    });
  }

  removeProduct(cartProductData: CartData): Observable<any> {
    return new Observable((observer) => {
      this.http.delete(`${this.apiUrl}/users/${cartProductData.account}/cart/${cartProductData.cart_id}`, { withCredentials: true }).subscribe((results: any) => {
        if (results.success) {
          this.loadUserCart(cartProductData.account).subscribe((productLoaded: any) => {
            if (productLoaded.success) {
              observer.next(results);
              observer.complete();
            }
          });
        }
      });
    });
  }
  
  checkout(account_id: number): Observable<any> {
    return new Observable((observer) => {
      this.http.delete(`${this.apiUrl}/users/${account_id}/cart/checkout`, { withCredentials: true }).subscribe((results: any) => {
        if (results.success) {
          this.loadUserCart(account_id).subscribe();
        }
        observer.next(results);
        observer.complete();
      });
    });
  }

  get totalPrice(): number {
    let totalPrice = 0;
    this.cartProducts.forEach((cartProduct) => {
      totalPrice += cartProduct.product_price * cartProduct.count;
    });
    return Math.round((totalPrice + Number.EPSILON) * 100) / 100;
  }

}
