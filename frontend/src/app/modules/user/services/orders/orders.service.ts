import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


interface OrderData {
  customer: number;
  total_price: number;
  payment: number;
  shipping: number;
  order_status: number;
  products: any[];
}


@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  apiUrl: string = environment.apiHost + environment.v1ApiUrl;
  ordersList: any[] = [];

  constructor(private http: HttpClient) { }

  loadUserOrders(account_id: number): Observable<boolean> {
    return new Observable((observer) => {
      this.http.get(`${this.apiUrl}/users/${account_id}/orders/`, { withCredentials: true }).subscribe((orders_list: any) => {
        if (orders_list.success) {
          this.ordersList = orders_list.message;
          observer.next(orders_list.success);
          observer.complete();
        }
      });
    });
  }

  getOrderDetails(order_id: number, account_id: number): Observable<any[]> {
    return new Observable((observer) => {
      this.http.get(`${this.apiUrl}/users/${account_id}/orders/${order_id}`, { withCredentials: true }).subscribe((orderDetails: any) => {
        if (orderDetails.success) {
          observer.next(orderDetails);
          observer.complete();
        }
      });
    });
  }

  newOrder(orderData: OrderData, account_id: number): Observable<any> {
    return new Observable((observer) => {
      this.http.post(`${this.apiUrl}/users/${account_id}/orders/`, orderData, { withCredentials: true }).subscribe((results) => {
        observer.next(results);
        observer.complete();
      });
    });
  }

  rejectOrder(order_id: number, account_id: number): Observable<any> {
    return new Observable((observer) => {
      this.http.put(`${this.apiUrl}/users/${account_id}/orders/${order_id}/reject`, {}, { withCredentials: true }).subscribe((results: any) => {
        if (results.success) {
          this.loadUserOrders(account_id).subscribe();
        }
        observer.next(results);
        observer.complete();
      });
    })
  }

}
