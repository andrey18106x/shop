import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  apiUrl: string = environment.apiHost + environment.v1ApiUrl;
  usersApiURL: string = this.apiUrl + "/users";

  constructor(private http: HttpClient) { }

  getUserInfo(account_id: number): Observable<any> {
    return new Observable((observer) => {
      this.http.get(`${this.usersApiURL}/${account_id}`).subscribe((user_info) => {
        observer.next(user_info);
        observer.complete();
      });
    });
  }

}
