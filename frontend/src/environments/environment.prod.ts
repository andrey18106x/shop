export const environment = {
  production: true,
  domain: '.shop.com.ua',
  apiHost: "http://api.shop.com.ua:3030",
  v1ApiUrl: "/api/v1"
};
